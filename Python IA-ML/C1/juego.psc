Algoritmo juego
	derrota <- Falso
	Escribir "Vamos a jugar a un juego, responde `s� o `n�"
	
	Escribir "Colon descubrio America?"
	Leer resp1
	Si resp1 == "s" Entonces
		Escribir "La capital de Inglaterra es Cambridge?"
		Leer resp2
		Si resp2 == "n" Entonces
			Escribir "El sol es una estrella?"
			Leer resp3
			Si resp3 == "s" Entonces
				Escribir "Has gando el juego!"
				derrota <- Verdadero
			FinSi
		FinSi
	Fin Si
	//El simbolo de ! es igual que poner "!derrota -> derrota == false", si pusieras solo el nombre de la variable seria "derrota -> derrota == true"
	Si !derrota Entonces
		Escribir "Se acabo el juego!"
	Fin Si
FinAlgoritmo
