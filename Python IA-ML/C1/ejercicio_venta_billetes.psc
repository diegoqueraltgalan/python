Funcion billetes_v<-venta_billetes (capacidad_bus,billetes_vendidos)
	Escribir " ¿Cuantos billetes desea comprar? (" capacidad_bus-billetes_vendidos " billetes disponibles) "
	Leer comp_billetes
	res = billetes_vendidos+comp_billetes
	Si  res <= capacidad_bus Entonces
		Escribir "Operacion realizada"
		billetes_v = res
	SiNo
		Escribir "ERROR NO PUEDE COMPRAR TANTOS BILLETES"
		billetes_v = billetes_vendidos
	FinSi
Fin Funcion


Funcion billetes_d <- devolucion_billetes ( capacidad_bus,billetes_vendidos )
	
	Escribir "¿Cuantos billetes desea devolver?"
	Leer dev_billetes
	Si dev_billetes>capacidad_bus O dev_billetes>billetes_vendidos entonces
		Escribir "ERROR NO PUEDE DEVOLVER TANTOS BILLETES"
		billetes_d = billetes_vendidos
	SiNo
		Escribir "Operacion realizada"
		billetes_d = billetes_vendidos-dev_billetes
		billetes_disponibles = capacidad_bus - billetes_d
		Escribir "Ahora quedan " billetes_disponibles " billetes disponibles"
	FinSi
	
Fin Funcion


Funcion estado_venta (capacidad_bus, billetes_vendidos)
	
	Escribir "Numero de plazas totales: " capacidad_bus
	Escribir "Numero de plazas vendidas: " billetes_vendidos
	Escribir "Numero de plazas disponibles: " capacidad_bus-billetes_vendidos
	
Fin Funcion

	
	
	Algoritmo compra_venta_billetes
		capacidad_bus = 65
		billetes_vendidos = 0
		Repetir
			Escribir "---MENU---"
			Escribir "1.- Venta billetes"
			Escribir "2.- Devolver billetes"
			Escribir "3.- Estado venta"
			Escribir "0.- Salir"
			Escribir "Escribe una opcion: "
			Leer op
			Segun op Hacer
				1:
					billetes_vendidos = venta_billetes(capacidad_bus, billetes_vendidos)
				2:
					billetes_vendidos = devolucion_billetes(capacidad_bus, billetes_vendidos)
				3:
					estado_venta(capacidad_bus, billetes_vendidos)
				0: 
					Escribir "Adios!"
				De Otro Modo:
					Escribir "No existe esta opcion."
			Fin Segun
		Hasta Que op == 0
FinAlgoritmo
