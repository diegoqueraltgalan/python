//Introducir un numero desde teckadi y visuaizar por pantalla su tabla de multiplicar
Algoritmo tabla_multiplicar
	Leer numero
	Para mult<-0 Hasta 10 Con Paso 1 Hacer
		Escribir numero " * " mult " = " numero*mult
	Fin Para
	
FinAlgoritmo
