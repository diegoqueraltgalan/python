//Imprimir los numeros pares hasta el 100 y que imprima cuantos pares hay
Algoritmo numeros_pares_0_100
	total = 0
	Para numero<-0 Hasta 100 Con Paso 2 Hacer
		Escribir numero
		total = total + 1
	Fin Para
	
	Escribir "Hay " total " numeros pares"
FinAlgoritmo
