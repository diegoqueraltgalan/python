//Menu opciones, lista 3 opciones (1,2,3) y opcion para salir
Algoritmo sin_titulo
	op = 0
	Repetir
		Escribir "---MENU---"
		Escribir "1.- Opcion 1"
		Escribir "2.- Opcion 2"
		Escribir "3.- Opcion 3"
		Escribir "Escribe una opcion: "
		Leer op
		Segun op Hacer
			1:
				Escribir "Escogio la opcion 1."
			2:
				Escribir "Escogio la opcion 2."
			3:
				Escribir "Escogio la opcion 3."
			0: 
				Escribir "Se acabo!"
			De Otro Modo:
				Escribir "No existe esta opcion."
		Fin Segun
	Hasta Que op == 0
FinAlgoritmo
