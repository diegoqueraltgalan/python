Funcion que_es <- vocal_o_consonante ( letra )
	letra <- Minusculas(letra)
	Segun letra Hacer
		"a","e","i","o","u":
			que_es = "Vocal"
		De Otro Modo:
			que_es = "Consonante"
	Fin Segun
	
Fin Funcion

Algoritmo sin_titulo
	Escribir "Escribe una letra"
	Leer letra
	Escribir vocal_o_consonante(letra)
FinAlgoritmo
