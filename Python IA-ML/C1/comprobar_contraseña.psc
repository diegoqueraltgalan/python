Algoritmo comprobar_contraseņa
	contraseņa_correcta = "admin"
	cont_intentos = 0
	max_intentos = 3
	Repetir
		Escribir "Introduce la contraseņa: "
		Leer contraseņa_introducida
		Si contraseņa_introducida!=contraseņa_correcta Entonces
			cont_intentos = cont_intentos + 1
			Escribir "Contraseņa incorrecta, te quedan "  max_intentos - cont_intentos " intentos."
		SiNo 
			Escribir "Contraseņa correcta."
		FinSi
		
	Hasta Que contraseņa_introducida == contraseņa_correcta O cont_intentos >= max_intentos
	
	
FinAlgoritmo
