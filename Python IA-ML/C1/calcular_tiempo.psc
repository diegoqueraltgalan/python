Algoritmo sin_titulo
	Escribir "Escribe un numero en segundos: "
	Leer seg
	h <- 0
	min <- 0
	Si seg>60 Entonces
		
		min = trunc(seg/60)
		seg = seg MOD 60
		
		Si min>60 Entonces
			h = trunc(min/60)
			min = min MOD 60	
		FinSi
	Fin Si
	
	Escribir h " h " min " m " seg " s " 
FinAlgoritmo
