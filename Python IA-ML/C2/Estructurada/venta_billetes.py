def estado ( plazas_ocupadas,total_plazas ):
    plazas_libres=total_plazas-plazas_ocupadas
    print("Total de Plazas: ",total_plazas)
    print("Total Plazas Libres: ",plazas_libres)
    print("Total Plazas Ocupadas: ",plazas_ocupadas)

def devolucion ( plazas_ocupadas, numero_billetes_devolver ):

    if numero_billetes_devolver <= plazas_ocupadas:
        print("Devolución realizada con éxito.")
        plazas_ocupadas = plazas_ocupadas - numero_billetes_devolver
    else:
        print("No se puede devolver esa cantidad de billetes.")
	
    return plazas_ocupadas

def venta ( total_plazas, plazas_ocupadas, numero_billetes ):
	
    if(numero_billetes <= (total_plazas-plazas_ocupadas)):
        print("Transacción realizada.")
        print("Gracias Por Su Compra.")
        plazas_ocupadas = plazas_ocupadas + numero_billetes
    else:
        print("No hay plazas suficientes.")

    return plazas_ocupadas


total_plazas = int(input("Introduce el número total de plazas: "))

plazas_ocupadas = 0
opcion = -1

while (opcion != 0):
    print("""        
***********AI Transportes***************"
         1. Venta de billetes
       2. Devolución de billetes
         3. Estado de la venta
             0. Salir
****************************************
""")
    opcion = int(input("Elige una opción: "))
    
    if opcion == 1:
        numero_billetes = int(input("Introduce el número de billetes que quieres comprar: "))
        plazas_ocupadas = venta(total_plazas, plazas_ocupadas, numero_billetes)
    elif opcion == 2:
        numero_billetes_devolver = int(input("Introduce el número de billetes que quieres devolver: "))
        plazas_ocupadas = devolucion(plazas_ocupadas, numero_billetes_devolver)
    elif opcion == 3:
        estado(plazas_ocupadas, total_plazas)        
    elif opcion == 0:
        print("ADIOH!")
    else:
        print("Aprende a leer :D")
