numero = int(input("Escribe un numero (base decimal y menor de 4000): "))

letras_m = 0
letras_d = 0
letras_c = 0
letras_l = 0
letras_x = 0
letras_v = 0
letras_i = 0
cadena_romano = ""

#base 1000
letras_m = int(numero/1000)
if letras_m>0:
	numero = numero - letras_m*1000
	cadena_romano += "M"*letras_m
print(numero," ",cadena_romano)

#Base 500
letras_d = int(numero/500)
if letras_d>0:
	numero = numero - letras_d*500

	if letras_d == 4:
		cadena_romano += "DM"
	else:
		cadena_romano += "D"*letras_d
cadena_romano = cadena_romano.replace("MDM","MD")
print(numero," ",cadena_romano)

#base 100
letras_c = int(numero/100)
if letras_c>0:
	numero = numero - letras_c*100

	if letras_c == 4:
		cadena_romano += "CD"
	else:
		cadena_romano += "C"*letras_c
cadena_romano = cadena_romano.replace("DCD","CM")
print(numero," ",cadena_romano)

#base 50
letras_l = int(numero/50)
if letras_l>0:
	numero = numero - letras_l*50

	if letras_l == 4:
		cadena_romano += "LC"
	else:
		cadena_romano += "L"*letras_l

cadena_romano = cadena_romano.replace("CLC","CL")
print(numero," ",cadena_romano)

#base 10
letras_x = int(numero/10)
if letras_x>0:
	numero = numero - letras_x*10

	if letras_x == 4:
		cadena_romano += "XL"
	else:
		cadena_romano += "X"*letras_x
cadena_romano = cadena_romano.replace("LXL","XC")
print(numero," ",cadena_romano)


#base 5
letras_v = int(numero/5)
if letras_v>0:
	numero = numero - letras_v*5

	if letras_v == 4:
		cadena_romano += "VX"
	else:
		cadena_romano += "V"*letras_v
cadena_romano = cadena_romano.replace("XVX","XV")
print(numero," ",cadena_romano)

#base 1
letras_i = int(numero)
if letras_i>0:
	numero = numero - letras_i

	if letras_i == 4:
		cadena_romano += "IV"
	else:
		cadena_romano += "I"*letras_i
cadena_romano = cadena_romano.replace("VIV","IX")
print(numero," ",cadena_romano)

print(cadena_romano)