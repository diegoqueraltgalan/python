
contraseña = ""
contraseña_correcta = False
vocales = "AEIOU"
numeros = "0123456789"
while not contraseña_correcta: 
	contraseña = input("Introduce una contraseña: ")
	if len(contraseña) >= 7:
		if contraseña[0] in vocales:
			if contraseña[-1] in numeros:
				print("Contraseña correcta")
				contraseña_correcta = True
			else:
				print("Contraseña incorrecta, debe acabar en numero")
		else:
			print("Contraseña incorrecta, debe empezar por vocal mayuscula")
	else:
		print("Contraseña incorrecta, debe tener 7 caracteres o mas")
