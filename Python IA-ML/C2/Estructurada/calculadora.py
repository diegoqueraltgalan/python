import math

def sumar_dos_numeros(num1,num2):
    return num1 + num2

def restar_dos_numeros(num1, num2):
    return num1 - num2

def multiplicar_dos_numeros(num1, num2):
    return num1 * num2

def dividir_dos_numeros(num1, num2):
    return num1 / num2

def raiz_cuadrada(num):
    return math.sqrt(num)

def cuadrado(num):
    return num*num

opcion = -1

while (opcion != 0):

    print("*****************")
    print("1. Sumar")
    print("2. Restar")
    print("3. Multiplicar")
    print("4. Dividir")
    print("5. Raiz cuadrada")
    print("6. Cuadrado")
    print("0. Salir")
    print("*****************")

    opcion = int(input("Introduce una opción: "))
    if opcion!=0 and opcion<=6: 
        num = int(input("Introduce un numero: "))
        if (opcion<5):
            num2 = int(input("Introduce otro numero: "))
               
    if opcion == 1:
        print(sumar_dos_numeros(num,num2))
    elif opcion == 2:
        print(restar_dos_numeros(num,num2))
                
    elif opcion == 3:
        print(multiplicar_dos_numeros(num,num2))
    elif opcion == 4:
        print(dividir_dos_numeros(num,num2))
    elif opcion == 5:
        print(raiz_cuadrada(num))
    elif opcion == 6:
        print(cuadrado(num))        
    elif opcion == 0:
        print("ADIOH!")
    else:
        print("Aprende a leer :D")
