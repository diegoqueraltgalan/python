import math

class Triangulo:
    def __init__(self, tBase,tAltura,x,y):
        self.__base = tBase
        self.__altura = tAltura
        self.__coordenada = Coordenada(x,y)
    def calcularArea(self):
        return ((self.__base*self.__altura)/2)
    def setBase(self,tBase):
        if tBase <= 0:
            print("El valor debe ser positivo")
        else:
            self.__base = tBase
    def getBase(self,tBase):
        return self.__base
    def setAltura(self,tAltura):
        if tAltura <= 0:
            print("El valor debe ser positivo")
        else:
            self.__altura = tAltura
    def getAltura(self,tAltura):
        return self.__altura
    def getCoordenada(self):
        return self.__coordenada
    def setCoordenada(self,x,y):
        self.__coordenada = Coordenada(x,y)

class Cuadrado:
    def __init__(self, cBase, cAltura, x):
        self.__base = cBase
        self.__coordenada = Coordenda(x,y)
    def calcularArea(self):
        return (self.__base**2)
    def setBase(self,cBase):
        if cBase <= 0:
            print("El valor debe ser positivo")
        else:
            self.__base = cBase
    def getBase(self,cBase):
        return self.__base
    def getCoordenada(self):
        return self.__coordenada
    def setCoordenada(self,x,y):
        self.__coordenada = Coordenada(x,y)


class Rectangulo:
    def __init__(self, cBase, cAltura, x, y):
        self.__base = cBase
        self.__altura = cAltura
        self.__coordenada = Coordenda(x,y)
    def calcularArea(self):
        return (self.__base*self.__altura)
    def setBase(self,cBase):
        if cBase <= 0:
            print("El valor debe ser positivo")
        else:
            self.__base = cBase
    def getBase(self,cBase):
        return self.__base
    def setAltura(self,cAltura):
        if cAltura <= 0:
            print("El valor debe ser positivo")
        else:
            self.__altura = cAltura
    def getAltura(self,cAltura):
        return self.__altura
    def getCoordenada(self):
        return self.__coordenada
    def setCoordenada(self,x,y):
        self.__coordenada = Coordenada(x,y)


class Circulo:
    def __init__(self, cRadio, x, y):
        self.__radio = cRadio
        self.__coordenada = Coordenada(x,y)
    def calcularArea(self):
        return (self.__radio*self.__radio)
    def setRadio(self,cRadio):
        if cRadio <= 0:
            print("El valor debe ser positivo")
        else:
            self.__radio = cRadio
    def getAltura(self,cRadio):
        return self.__radio
    def setCoordenada(self,x,y):
        self.__coordenada = Coordenada(x,y)
    def getCoordenada(self):
        return self.__coordenada
        
class Coordenada:
    def __init__(self,x,y,z):
        self.__x = x
        self.__y = y
        self.__z = z
    def getX(self):
        return self.__x
    def setX(self,x):
        if type(x) is int or type(x) is float:
            return x
        else:
            print("El valor no es un número")
    def getY(self):
        return self.__y
    def setY(self,Y):
        if type(y) is int or type(y) is float:
            return y
        else:
            print("El valor no es un número")
    def getZ(self):
        return self.__z
    def setZ(self,z):
        if type(z) is int or type(z) is float:
            return z
        else:
            print("El valor no es un número")
